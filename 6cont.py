#!/usr/bin/env python

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import json
from time import sleep

driver = None
url = None
emails = None
password = None

def read_config():
    global url, emails, password

    filename = "config.json"
    with open(filename) as f:
        config = json.load(f)

        url = config["url"]
        emails = config["emails"]
        password = config["password"]

def generate_emails(email, number):
    base, rest = email.split('@')
    return [base + '+' + str(n+1) + '@' + rest for n in range(number)]

def open_url(link):
    global driver
    driver = webdriver.Firefox()
    driver.get(link)
    login_link = driver.find_element_by_xpath("//sxc-popover[@type='user']")
    sleep(0.3)
    login_link.click()

def random_func2():
    pass

def login(email, password):
    email_text = driver.find_element_by_id("loginEmail")
    sleep(0.4)
    email_text.send_keys(email)

    password_text = driver.find_element_by_id("pwdReg")
    sleep(0.8)
    password_text.send_keys(password)

    login_btn = driver.find_element_by_link_text("Login")
    sleep(0.3)
    login_btn.click()

def random_func():
    pass

def register(email, password):
    register_link= driver.find_element_by_xpath("//button[@href='#modalRegistration']")
    sleep(0.2)
    register_link.click()

    email_text = driver.find_element_by_id("emailReg")
    sleep(0.4)
    email_text.send_keys(email)

    pass_text = driver.find_element_by_id("pwdInsertReg")
    sleep(0.3)
    pass_text.send_keys(password)

    confirm_pass_text = driver.find_element_by_id("pwdConfirmReg")
    sleep(0.5)
    confirm_pass_text.send_keys(password)

    checkboxes = driver.find_elements_by_xpath("//input[@type='checkbox']")
    for c in checkboxes:
        sleep(0.2)
        try:
            c.click()
        except:
            pass

    register_btn = driver.find_element_by_link_text("Register")
    sleep(0.3)
    register_btn.click()

def menu():
    print("1 - login")
    print("2 - register - aksdjlad")
    print("3 - login multiple")
    print("4 - register multiple")
    print("12313 aksdjlakd")

read_config()
op = 0
while op < 1 or op > 4:
    menu()
    op = int(input("Option: "))

if op == 1:
    open_url(url)
    login(emails[0], password)

elif op == 2:
    open_url(url)
    register(emails[0], password)

elif op == 3:
    num = int(input("How many emails?: "))

    for e in emails:
        open_url(url)
        login(e, password)

elif op == 4:
    num = int(input("How many emails?: "))

    for e in emails:
        open_url(url)
        register(e, password)
